# Cheat sheet golang net/http

## Query Params
Berikut cara mengambil parameter query pada modul net/http :

1. Menggunakan request.URL.Query(). <br/>Query params terdapat pada **URL** itu sendiri, query param dapat didapat menggunakan r.URL.Query() yang mengembalikan **Values** dengan tipe *map\[string\][]string*.

    1.1 Ketika suatu parameter memiliki banyak value.


            // http://localhost:8080/api/products?filters=color&filters=price&filters=brand

            func getProducts(w http.ResponseWriter, r *http.Request) {
                query := r.URL.Query()
                filters, present := query["filters"] // filters=["color","price","brand"]
                if !present || len(filters) == 0 {
                    fmt.Println("filters not present")
                }

                w.WriteHeader(200)
                w.Write([]byte(strings.Join(filters, ",")))
            }

    1.2 Ketika suatu parameter memiliki satu value.

            // http://localhost:8080/api/products/filters=color

            func getProducts(w http.ResponseWriter, r *http.Request) {
                query := r.URL.Query()
                filters := query.Get("filters") // filters="color"
                w.WriteHeader(200)
                w.Write([]byte(filters))
            }

## Request Body Parsing
Pada http method **POST** dan **PUT**, biasanya terdapat form body. Content-Type yang digunakan untuk mengirim form body yaitu :

- application/x-www-form-urlencoded
- multipart/form-data (untuk submit file)

berikut cara untuk mengambil data dari form :

```
func createEmployee(w http.ResponseWriter, request *http.Request) {
	err := request.ParseMultipartForm(32 << 20) // maxMemory 32MB
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	
	fmt.Println(request.Form["name"]) // ["John"]
	fmt.Println(request.PostForm["name"]) // ["John"]
	fmt.Println(request.MultipartForm.Value["name"]) // ["John"]
	fmt.Println(request.PostFormValue("name")) // John

	fmt.Println(request.Form["age"]) // [23]
	fmt.Println(request.PostForm["age"]) // [23]
	fmt.Println(request.MultipartForm.Value["age"]) // [23]
	fmt.Println(request.PostFormValue("age")) // 23


	//Access the photo key - First Approach
	_, h, err := request.FormFile("photo")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	saveFile(h, "formfile")

	//Access the photo key - Second Approach
	for _, h := range request.MultipartForm.File["photo"] {
		err := saveFile(h, "mapaccess")
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	w.WriteHeader(200)
	return
}
```

