# Cheat sheet golang net/http testing


## Definisi testing untuk http handler

Testing http handler berfungsi untuk memeriksa fungsionalitas handler secara khusus diluar proses bisnis aplikasi.
contoh http testing :

``` golang
// Example 1
func TestSearchHandlerReturnsBadRequestWhenNoSearchCriteriaIsSent (t *testing.T) {
    handler := SearchHandler{}
    request := httptest.NewRequest("GET","/search",nil)
    response := httptest.NewRecorder()

    handler.ServeHTTP(response, request)
    if response.Code != http.StatusBadRequest {
        t.Errorf("Expected BadRequest got %v", response.code)
    }
}

// Example 2
func TestSearchHandlerReturnsBadRequestWhenBlankSearchCriteriaIsSent (t *testing.T) {
    handler := SearchHandler{}
    data, _ := json.Marshal(searchRequest{})
    request := httptest.NewRequest("POST", "/search", bytes.NewReader(data))

    response := httptest.NewRecorder()
    handler.ServeHTTP(response, request)
    if response.Code != http.StatusBadRequest {
        t.Errorf("Expected BadRequest got %v", response.code)
    }
}
```

Berikut perintah untuk menjalankan testing pada project golang :

``` bash
go test -cover ./... # with coverage
go test ./... # without coverage
```

