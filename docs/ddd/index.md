# Domain Driven Desain

## Bagian I - Desain Strategis

### Chapter 1. Analisis Domain Bisnis

> There is no sense in talking about the solution before we agree on the problem,
> and no sense talking about the implementation steps before
> we agree on the solusion. - Efrat Goldratt-Ashlag

**Business Domain** merupakan definisi dari aktifitas utama perusahaan. Atau pelayanan yang diberikan perusahaan kepada client.

**Sub-domain** adalah area aktivitas bisnis yang terperinci.

#### Tipe dari sub-domain

***code subdomain*** adalah inti yang membedakan perusahaan dengan kompetitor.

***generic subdomain*** adalah aktifitas bisnis yang dilakukan semua perusahaan dengan cara yang sama.

***supporting subdomain*** adalah aktifitas yang mendukung bisnis perusahaan tapi tidak memberikan keuntungan kompetitif.

#### Istilah lainnya
*domain experts* adalah orang yang ahli pada bidang atau domain tertentu.

### Chapter 2. Menemukan Domain Knowledge
Komunikasi yang efektif dan sharing knowledge sangat krusial untuk keberhasilan proyek software. Software enginer harus memahami domain bisnis untuk bisa mendesain dan membangun software solution.

***Domain-Driven Design ubiquitous language*** adalah alat untuk menjebatani celah pengetahuan antara *domain experts* dan *software engineers*. 

***Ubiquitous language*** harus mengeliminasi ambiguitas dan asumsi tersembunyi. Semua istilah yang digunakan harus konsisten.

Tools lainnya untuk memberdayakan penggunaan *ubiquitous language* adalah glosary berbasis wiki dan gherkin tests.

### Chapter 3. Mengelola Kompleksitas Domain

> It’s developers’ (mis)understanding, not domain experts’ knowledge,
> that gets released in production. - Alberto Brandolini

> A model is a simplified representation of a thing or phenomenon that
> intentionally emphasizes certain aspects while ignoring others. Abstraction
> with a specific use in mind. - Rebecca Wirfs-Brock

Setiap kali menemukan konflik yang melekat dalam mental domain model para ahli, kita harus menguraikan *ubiquitous language* menjadi beberapa *bounded context*.
*Ubiquitous language* harus konsisten dalam lingkup *bounded context*. Namun, di seluruh  *bounded context*, istilah yang sama dapat memiliki perbedaan arti.

Sementara subdomain ditemukan, *bounded context* dirancang. Pembagian domain ke dalam *bounded context* adalah keputusan desain strategis.

*Bounded context* dan *ubiquitous language* dapat diimplementasikan dan dipelihara oleh satu tim. 
Tidak ada dua tim yang dapat berbagi pekerjaan pada *bounded context* yang sama. Namun, satu tim dapat bekerja pada beberapa *bounded context*.

*Bounded context* menguraikan sistem menjadi komponen fisik, services, subsistem, dan sebagainya. Siklus hidup setiap *bounded context* dipisahkan dari yang lain.
Setiap *bounded context* dapat berkembang secara independen dari sistem lainnya. Namun, *bounded context* harus bekerja sama untuk membentuk suatu sistem. Beberapa perubahan
secara tidak sengaja akan mempengaruhi *bounded context* lainnya.

### Chapter 4. Integrasi Bounded Context

*Bounded context* tidak independen. Mereka harus berinteraksi satu sama lain. Pola berikut menentukan cara *bounded context* dapat diintegrasikan:

**Partnership** - *Bounded context* diintegrasikan secara ad hoc.

**Shared Kernel** - Dua atau lebih *bounded context* diintegrasikan dengan berbagi model tumpang tindih terbatas yang dimiliki oleh semua *bounded context* yang berpartisipasi.

**Conformist** - Konsumen menyesuaikan diri dengan model penyedia layanan.

**Anti-Corruption Layer** - Konsumen menerjemahkan model penyedia layanan menjadi model yang sesuai dengan kebutuhan konsumen.

**Open-Host Service** - Penyedia layanan mengimplementasikan *published language* yang dioptimalkan untuknya kebutuhan konsumen.

**Separate Ways** - Lebih murah untuk menduplikasi fungsi tertentu daripada berkolaborasi dan mengintegrasikannya.

## Bagian II - 
## Bagian III -