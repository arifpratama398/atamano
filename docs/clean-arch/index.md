# Rangkuman Clean Architecture 

Catatan ini mereferensi pada repository git [open source](https://github.com/irahardianto/service-pattern-go) tentang clean architecture.

## Tujuan Clean Architecture

- **Independent of Framework**, system yang dibuat harus bisa menjadi sistem yang independen, tidak terikat oleh framework. Framework digunakan untuk mendukung implementasi system daripada membatasi kapabilitas sistem.
- **Highly Testable**, membantu dalam test sistem, diharapkan dapat mencover lapisan sebanyak mungkin untuk memastikan code reliability.
- **Independent of Database**, business logic seharusnya tidak terikat pada database.
- **Independent of 3rd party library**, tidak ada 3rd party yang diimplementasikan secara langsung di business logic, perlu dilakukan abstracsi agar lebih mudah mengubah libary kapan saja.


## Stuktur Folder 

```bash
/
|- controllers
|- infrastructures
|- interfaces
|- models
|- repositories
|- services
|- viewmodels
main.go
router.go
servicecontainer.go
```

Struktur folder ini dibuat untuk mengakomodasi `separation of concern principle`, dimana satu struct hanya memiliki satu tugas untuk mencapai sistem terpisah.

### controllers
folder ini menyimpan semua struct yang berisi handler dari semua request. business logic dan data access layer akan ditangani secara terpisah.

### infrastructures
folder ini menyimpan setup sistem untuk melakukan komunikasi dengan database atau eksternal datasource lainnya.

### interfaces
folder ini menyimpan interface, interface seperti namanya adalah jembatan antara domain yang berbeda sehingga mereka dapat berinteraksi satu sama lain, dalam kasus kami, ini harus menjadi satu-satunya cara bagi mereka untuk berinteraksi.

### models
folder ini menyimpan struct-struct yang berfungsi sebagai entity dalam sebuah aplikasi.

### repositories
folder ini menyimpan program repositori, repositori adalah tempat implementasi lapisan akses data. Semua kueri dan operasi data dari / ke basis data harus terjadi di sini, dan pelaksana harus agnostik tentang mesin basis data apa yang digunakan, bagaimana kueri dilakukan, yang mereka pedulikan hanyalah mereka dapat menarik data sesuai dengan antarmuka yang mereka implementasikan .

### services
folder ini menyimpan business logic, menangani request dari controller dan mengambil data dari data akses layer.

controller mungkin mengimplementasikan banyak interface service untuk memenuhi kebutuhan permintaan, dan pengontrol harus agnostik tentang bagaimana layanan mengimplementasikan logika mereka, yang mereka pedulikan adalah mereka harus dapat menarik hasil yang mereka butuhkan sesuai dengan antarmuka yang mereka implementasikan.

### viewmodels
viewmodels adalah model yang akan digunakan sebagai pengembalian respons dari panggilan REST API

### main.go
file entry point system.

### router.go
file ini berisi tentang bagaimana merutekan setiap controller menjadi http handler.

### servicecontainer.go
file ini berisi tentang bagaimana aplikasi dalam mengisi implementasi dari interface.
