# Setup Server Development

## Install Development Nats Server using Docker.

Perintah untuk menjalankan nats, dalam docker :

``` bash
$ docker run --name nats --rm -p 4222:4222 -p 8222:8222 nats
```

> Note : server diatas bersifat temporary, dan akan dihapus ketika server dimatikan


## Install nats-top di komputer local.

Perintah untuk menginstall nats-top, di local :
``` bash
$ go get github.com/nats-io/nats-top
```

> Note : package akan menghilang ketika melakukan update versi golang di lokal.